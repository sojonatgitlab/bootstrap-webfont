<h3 align="center">Bootstrap Icons</h3>

<p align="center">
  UnOfficial open source Webfont icon library for Bootstrap.
  <br>
  <a href="https://icons.getbootstrap.com/"><strong>Explore Bootstrap Icons »</strong></a>
</p>

## Clone this project

```shell
git clone https://gitlab.com/sojonatgitlab/bootstrap-webfont.git
```


## How to use

Depending on your setup, you can include Bootstrap Icons in a handful of ways.

- You need to copy the entire `font` directory into your project folder.
- In the <head> of your html, reference the location to your `bootstrap-webfont-v1.0.0.css`.
- Icon class names are same as Bootstrap icon name. <a href="https://icons.getbootstrap.com/"><strong>Explore Bootstrap Icons »</strong></a>

```shell
<link rel="stylesheet" href="css/bootstrap-webfont-v1.0.0.css">
```
```shell
<i class="bi bi-arrow-up-right"></i>
```


## License

MIT
